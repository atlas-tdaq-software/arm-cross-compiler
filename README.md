
Cross-compiler for ARM in a container
======================================

The tasks here are split into multiple steps.

1. Building a basic ARM CentOS 7 image
2. Building a ARM cross-compiler image using the previous image as root fs.

You can re-use the available ARM base image by going
directly to the step 'Building the cross-compilation environment'.
In this case you don't need the `qemu-aarch64-static` binary
and you don't have to setup your system in any special way.

System Preparations
-------------------

This requires that the machine where you execute the 
docker build for the ARM images has
support to execute  aarch64 binaries. This can be achieved
on a x86 based system by the following commands as root:

```bash
mkdir -p /usr/local/bin && cd /usr/local/bin
wget https://github.com/multiarch/qemu-user-static/releases/download/v4.0.0/qemu-aarch64-static
chmod a+x qemu-aarch64-static
cat >  /etc/binfmt.d/qemu-aarch64.conf <<EOF
:qemu-aarch64:M::\x7fELF\x02\x01\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02\x00\xb7\x00:\xff\xff\xff\xff\xff\xff\xff\x00\xff\xff\xff\xff\xff\xff\xff\xff\xfe\xff\xff\xff:/usr/local/bin/qemu-aarch64-static:
EOF
systemctl restart systemd-binfmt
```

Note that the path to the qemu executable must be the same
on the host and the image.

CentOS 8: If you can use a newer host with at least CentOS 8 you should change the the magic line above to (note the trailing flags):

```bash
:qemu-aarch64:M::\x7fELF\x02\x01\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02\x00\xb7\x00:\xff\xff\xff\xff\xff\xff\xff\x00\xff\xff\xff\xff\xff\xff\xff\xff\xfe\xff\xff\xff:/usr/local/bin/qemu-aarch64-static:FC
```

There is no need to add the qemu executable to the ARM image in this case. Just use the `Dockerfile.runtime` and `Dockerfile.devel` as
in the `Makefile`:

```bash
make devel
```

Ubuntu 20.10: 

```bash
apt install qemu-system-arm qemu-user-binfmt qemu-user-static
```

In `/usr/share/binfmts/qemu-aarch64` change the line to `fix_binary yes`. Run 
```
update-binfmts --disable qemu-aarch64
update-binfmts --import qemu-aarch64
update-binfmts --enable qemu-aarch64

```

Building the aarch64 CentOS 7 Base Image
----------------------------------------

The BASE_IMAGE is optional and defaults to this git repository with `arm-cc7-base:latest`
as name. You may have to run a `docker login gitlab-registry.cern.ch` first.

```bash
make devel [ BASE_IMAGE=<your_registry> ]
```

CentOS 

Running the aarch64 Base Image with Qemu
----------------------------------------

```bash
docker run -it gitlab-registry.cern.ch/atlas-tdaq-software/arm-cross-compiler/arm-cc7-base:latest bash
$ cat /etc/os-release
$ uname -a
```

Building the cross-compilation environment
-------------------------------------------

From the top level directory (again the BUILD_IMAGE is optional):

```bash
make build [ BUILD_IMAGE=<your_registry> ]
```

To do all previous steps in one go:

```bash
make all
```

Running the cross-compiler image
---------------------------------

```bash
% docker run -it -v /cvmfs:/cvmfs:ro,shared gitlab-registry.cern.ch/atlas-tdaq-software/arm-cross-compiler/arm-cross-build bash
```

CI Job
------

The .gitlab-ci.yml job does all of the above on every checkin. It uses a privileged gitlab runner
from CERN IT and will even work without the local qemu image.

Examples
--------

See https://gitlab.cern.ch/atlas-tdaq-software/tdaq-arm for an example use.
