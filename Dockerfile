#
# The x86_64 cross-compiler container
#

# Standard ATLAS TDAQ build container
FROM gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:centos7

RUN mkdir -p /arm/sysroot /arm/compiler

# Copy the ARM64 image into /arm/sysroot
COPY --from=gitlab-registry.cern.ch/atlas-tdaq-software/arm-cross-compiler/arm-cc7-base / /arm/sysroot/

# Install RPMs needed on host
RUN yum install -y java-1.8.0-openjdk-devel && yum clean all

# Install cross-compiler
RUN cd /arm/compiler && curl -s -L https://developer.arm.com/-/media/Files/downloads/gnu-a/8.3-2019.03/binrel/gcc-arm-8.3-2019.03-x86_64-aarch64-linux-gnu.tar.xz | tar Jxf - 

# Add some symbolic links for later cvmfs + cmake 
RUN mkdir -p /cvmfs && ln -s /cvmfs /arm/sysroot/cvmfs
RUN mkdir -p /arm/sysroot/arm && ln -s /arm/compiler /arm/sysroot/arm

# Add an example CMake toolchain file
ADD aarch64-toolchain.cmake /arm/aarch64-toolchain.cmake
