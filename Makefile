
RUNTIME_IMAGE=gitlab-registry.cern.ch/atlas-tdaq-software/arm-cross-compiler/arm-cc7-runtime
BASE_IMAGE=gitlab-registry.cern.ch/atlas-tdaq-software/arm-cross-compiler/arm-cc7-base
BUILD_IMAGE=gitlab-registry.cern.ch/atlas-tdaq-software/arm-cross-compiler/arm-cross-build

all:	build

qemu-aarch64-static:
	wget github.com/multiarch/qemu-user-static/releases/download/v4.0.0/qemu-aarch64-static
	chmod a+x qemu-aarch64-static

runtime:qemu-aarch64-static
	docker build -t $(RUNTIME_IMAGE) -f Dockerfile.runtime .

devel:	runtime devel-only

devel-only:
	docker build -t $(BASE_IMAGE) -f Dockerfile.devel .

build:  devel build-only

build-only:
	docker build -t $(BUILD_IMAGE) -f Dockerfile .

push:	
	docker push $(RUNTIME_IMAGE)
	docker push $(BASE_IMAGE)
	docker push $(BUILD_IMAGE)
