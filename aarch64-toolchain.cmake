set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm)

set(CMAKE_SYSROOT /arm/sysroot)

set(tools /arm/compiler/gcc-arm-8.3-2019.03-x86_64-aarch64-linux-gnu)
set(CMAKE_C_COMPILER ${tools}/bin/aarch64-linux-gnu-gcc)
set(CMAKE_CXX_COMPILER ${tools}/bin/aarch64-linux-gnu-g++)
set(CMAKE_Fortran_COMPILER ${tools}/bin/aarch64-linux-gnu-gfortran)

set( CMAKE_C_FLAGS " -isystem /arm/compiler/gcc-arm-8.3-2019.03-x86_64-aarch64-linux-gnu/aarch64-linux-gnu/libc/usr/include " CACHE STRING "CFLAGS" )
set( CMAKE_C_LINK_FLAGS "" CACHE STRING "LDFLAGS" )

set( CMAKE_CXX_FLAGS " -isystem /cvmfs/sft.cern.ch/lcg/contrib/gcc/8/aarch64-centos7/include/c++/8.3.0 -isystem /arm/compiler/gcc-arm-8.3-2019.03-x86_64-aarch64-linux-gnu/aarch64-linux-gnu/libc/usr/include " CACHE STRING "CXXFLAGS" )
set( CMAKE_CXX_LINK_FLAGS " /cvmfs/sft.cern.ch/lcg/contrib/gcc/8/aarch64-centos7/lib64/libstdc++.so " CACHE STRING "LDFLAGS" )

set( CMAKE_FIND_ROOT_PATH /arm/compiler/gcc-arm-8.3-2019.03-x86_64-aarch64-linux-gnu/aarch64-linux-gnu/libc /cvmfs/sft-nightlies.cern.ch/lcg/views/devARM/Tue/aarch64-centos7-gcc8-opt /arm/sysroot )

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE BOTH)
